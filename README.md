To dump the game files:
```
cargo run -- -r game.iso -o output
```
To decompress a game file:
```
cargo run -- -d -f file -o output
```
To compress a file to be usable in-game:
```
cargo run -- -c -f file -o output
```

A sizeable portion of the algorithms for parsing DAT files, file info, and DAT compression was derived from the following project https://github.com/GovanifY/RECOM_Toolkit.