extern crate cc;

fn main() {
    cc::Build::new()
        .file("src/c/dec_cmp.c")
        .compile("dec_cmp");
}
