// This file is adapted from https://github.com/GovanifY/RECOM_Toolkit.

#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>

typedef struct
{
    unsigned char* ptrBufferCurrent;
    unsigned char lastFlag;
    unsigned char lastByte;
} State;

typedef struct
{
    unsigned char* data;
    size_t len;
} Buffer;

int decBlock(State* state, int flagToCheck);

int encBlock(State* state, int Flag, int Mask);

void finalizeBlock(State* state);

Buffer* Encode(Buffer* buffer);

Buffer* Decode(Buffer* buffer);

/* Frees the buffer passed in as input.
 *
 * Precondition: The buffer and the buffer's `data` must
 * be allocated by `malloc`.
*/
void freeBuffer(Buffer* buffer);
