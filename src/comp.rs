// Adapted from code at https://github.com/GovanifY/RECOM_Toolkit.

use std::convert::TryFrom;
use libc::{c_uchar, size_t};

/// The size to reserve for the data output to prevent several reallocations.
const BUFFER_BLOCK_SIZE: usize = 0x10_0000;
/// The size of a compressed block.
const COMP_BLOCK_SIZE: usize = 0x1000;

struct DecState<'a> {
    pub block: &'a [u8],
    pub offset: usize,
    pub last_flag: u8,
    pub last_byte: u8
}

impl<'a> DecState<'a> {
    pub fn new(block: &'a [u8]) -> DecState<'a> {
        DecState {
            block,
            offset: 0,
            last_flag: 0,
            last_byte: 0
        }
    }
}

fn dec_helper(state: &mut DecState, flag_to_check: u8) -> u8 {
    let mut result = 0;
    for _ in 0..flag_to_check {
        state.last_flag >>= 1;
        if state.last_flag == 0 {
            state.last_flag = 128;
            state.last_byte = state.block[state.offset];
            state.offset += 1;
        }
        result <<= 1;
        if state.last_byte & state.last_flag != 0 {
            result += 1;
        }
    }
    result
}

pub fn decompress(compressed: &[u8]) -> Vec<u8> {
    let mut data = Vec::with_capacity(BUFFER_BLOCK_SIZE);

    for block in compressed.chunks(COMP_BLOCK_SIZE) {
        let mut i = 1;
        // dictionary for uncompressed data
        let mut dic = [0u8; 0x100];
        let mut state = DecState::new(block);

        loop {
            loop {
                // og: check if there is uncompressed data
                if dec_helper(&mut state, 1) == 0 {
                    break;
                }
                // og: read uncompressed data and store it into a dictionary
                let read_byte = dec_helper(&mut state, 8);
                dic[i & 0xFF] = read_byte;
                data.push(read_byte);
                i += 1;
            }
            // og: check if data should be read from dictionary
            let mut dic_index = dec_helper(&mut state, 8) as usize;
            if dic_index == 0 {
                break;
            }
            let mut pos_start = i;
            let repeat_count = dec_helper(&mut state, 4) as usize;
            for _ in 0..(repeat_count + 2) {
                let read_byte = dic[dic_index & 0xFF];
                dic[pos_start & 0xFF] = read_byte;
                data.push(read_byte);
                dic_index += 1;
                pos_start += 1;
            }
            i += repeat_count + 2;
        }
    }

    data
}

#[repr(C)]
struct Buffer {
    pub ptr: *const c_uchar,
    pub len: size_t
}

impl Buffer {
    /// Consumes (frees) `data.ptr` and the `data` passed in and returns the data as a vector.
    /// 
    /// Precondition: `data` and `data.ptr` must be heap allocated by `malloc`.
    pub unsafe fn into_vec(data: *mut Buffer) -> Vec<u8> {
        let Buffer { ptr, len } = *data;
        let len = usize::try_from(len).unwrap();
        let v = std::slice::from_raw_parts(ptr, len).to_vec();
        free_buffer(data);
        v
    }
}

#[link(name="dec_cmp")]
extern {
    #[link_name="Encode"]
    fn encode(data: *const Buffer) -> *mut Buffer;

    #[link_name="freeBuffer"]
    fn free_buffer(buffer: *mut Buffer);
}

// TODO: Probably later I might want to write this function in Rust
// but I'm lazy and the C ffi works for now so it's fine.
pub fn compress(data: &[u8]) -> Vec<u8> {
    let buffer = Buffer {
        ptr: data.as_ptr(),
        len: data.len()
    };
    unsafe {
        Buffer::into_vec(encode(&buffer as *const Buffer))
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    const TEST_COMP: &[u8] = include_bytes!("../test/EF0001.EFF.comp");
    const TEST_DECOMP: &[u8] = include_bytes!("../test/EF0001.EFF");

    #[test]
    fn compression_test() {
        assert_eq!(TEST_DECOMP.to_vec(), decompress(&compress(TEST_DECOMP)));
    }

    #[test]
    fn decompression_test() {
        assert_eq!(TEST_DECOMP.to_vec(), decompress(TEST_COMP));
    }
}
