use byteorder::{ReadBytesExt, LE};

use std::io;
use std::io::SeekFrom;
use std::io::prelude::*;

use crate::iso_consts::*;
use crate::util;

pub const SUB_HEADER_LEN: u64 = 0x20;
pub const SUB_FILE_HEADER_LEN: u64 = 0x30;

pub struct Subpackage {
    pub unk00: u32,
    pub sector: u32,
    pub sector_size: u32,
    pub unk0c: u32,
    pub contents: Vec<u8>
}

pub struct SubFile {
    pub name: String,
    pub decomp_size: u32,
    pub unk1c: [u8; 8],
    pub sector_size: u32,
    pub sector: u32,
    pub flag: u16,
    pub contents: Vec<u8>
}

impl SubFile {
    pub fn compressed(&self) -> bool {
        (self.flag >> 8) & 0xFF > 0
    }
}

/// Iterator over the subpackages of a recom DAT file.
pub struct SubIterator<T: Read + Seek> {
    dat_file: T,
    index: u64,
    len: u64
}

impl<T: Read + Seek> SubIterator<T> {
    pub fn new(mut dat_file: T) -> io::Result<Self> {
        Ok(Self {
            len: subpackage_count(&mut dat_file)?,
            dat_file,
            index: 0
        })
    }
}

impl<T: Read + Seek> Iterator for SubIterator<T> {
    type Item = io::Result<Subpackage>;

    fn next(&mut self) -> Option<Self::Item> {
        if self.index < self.len {
            let result = get_subpackage(&mut self.dat_file, self.index);
            self.index += 1;

            Some(result)
        } else {
            None
        }
    }
}

/// Iterator over the files inside of a DAT subpackage.
pub struct SubFileIterator<T: Read + Seek> {
    subpackage: T,
    index: u64,
    len: u64
}

impl<T: Read + Seek> SubFileIterator<T> {
    pub fn new(mut subpackage: T) -> io::Result<Self> {
        Ok(Self {
            len: sub_file_count(&mut subpackage)?,
            subpackage,
            index: 0
        })
    }
}

impl<T: Read + Seek> Iterator for SubFileIterator<T> {
    type Item = io::Result<SubFile>;

    fn next(&mut self) -> Option<Self::Item> {
        if self.index < self.len {
            let result = get_sub_file(&mut self.subpackage, self.index);
            self.index += 1;

            Some(result)
        } else {
            None
        }
    }
}

pub fn subpackage_count(dat_file: &mut (impl Read + Seek)) -> io::Result<u64> {
    dat_file.seek(SeekFrom::Start(0))?;
    let mut count = 0;

    while dat_file.read_u32::<LE>()? > 0 {
        count += 1;
        dat_file.seek(SeekFrom::Start(count * SUB_HEADER_LEN))?;
    }

    Ok(count)
}

pub fn get_subpackage(dat_file: &mut (impl Read + Seek), index: u64) -> io::Result<Subpackage> {
    assert!(index < subpackage_count(dat_file)?, "Subpackage index out of range!");
    dat_file.seek(SeekFrom::Start(index * SUB_HEADER_LEN))?;

    // Read subpackage info
    let unk00 = dat_file.read_u32::<LE>()?; // Possibly the id of the subpackage?
    let sector = dat_file.read_u32::<LE>()?;
    let sector_size = dat_file.read_u32::<LE>()?;
    let unk0c = dat_file.read_u32::<LE>()?;

    let offset = u64::from(sector) * SECTOR_SIZE;
    let length = ((u64::from(sector_size) + 1) * SECTOR_SIZE) as usize;

    dat_file.seek(SeekFrom::Start(offset))?;
    let contents = util::read_bytes(dat_file, length)?;
    Ok(Subpackage {
        unk00,
        sector,
        sector_size,
        unk0c,
        contents
    })
}

pub fn is_subpackage_padding(subpackage: &mut (impl Read + Seek)) -> io::Result<bool> {
    subpackage.seek(SeekFrom::Start(0))?;
    let text = util::read_str(subpackage, 24)?;
    Ok(text.is_empty())
}

pub fn sub_file_count(subpackage: &mut (impl Read + Seek)) -> io::Result<u64> {
    subpackage.seek(SeekFrom::Start(0))?;
    let mut count = 0;

    while !util::read_str(subpackage, 24)?.is_empty() {
        count += 1;
        subpackage.seek(SeekFrom::Start(count * SUB_FILE_HEADER_LEN))?;
    }

    Ok(count)
}

pub fn get_sub_file(
    subpackage: &mut (impl Read + Seek),
    index: u64
) -> io::Result<SubFile> {
    assert!(index < sub_file_count(subpackage)?, "Subfile index out of range!");
    subpackage.seek(SeekFrom::Start(index * SUB_FILE_HEADER_LEN))?;

    // Get subfile info
    let name = util::read_str(subpackage, 24)?;
    let decomp_size = subpackage.read_u32::<LE>()?;
    let unk1c = read_bytes_array!(subpackage, 8)?;
    let sector_size = subpackage.read_u32::<LE>()?;
    let sector = subpackage.read_u32::<LE>()?;
    let flag = subpackage.read_u16::<LE>()?;

    let size = u64::from(sector_size) * SECTOR_SIZE;
    let offset = u64::from(sector) * SECTOR_SIZE;

    subpackage.seek(SeekFrom::Start(offset))?;

    let contents = {
        let mut buf = vec![0; size as usize];
        // Used instead of `read_exact` because some files are seemingly 0x10 bytes too short.
        let _ = subpackage.read(&mut buf)?;
        buf
    };

    Ok(SubFile {
        name,
        decomp_size,
        unk1c,
        sector_size,
        sector,
        flag,
        contents
    })
}
