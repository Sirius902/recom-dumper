pub const SECTOR_SIZE: u64 = 0x800;
pub const FILE_ENTRY_SIZE: u64 = 0x20;

pub const FILE_TABLE_PTR: u64 = 0x12_2800;
pub const FILE_TABLE_LEN_PTR: u64 = 0x12_2006;
