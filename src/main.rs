#![feature(seek_convenience, option_result_contains)]
extern crate byteorder;
use byteorder::{ReadBytesExt, LE};

extern crate clap;
use clap::{Arg, App};

extern crate libc;

use std::fs;
use std::io;
use std::io::SeekFrom;
use std::io::prelude::*;
use std::path::Path;

mod iso_consts;
use iso_consts::*;

#[macro_use]
mod util;

mod dat;
mod comp;

/// Dumps the files of recom with the game image `rom` to the directory `output`.
fn dump_rom(rom: impl AsRef<Path>, out: impl AsRef<Path>) -> io::Result<()> {
    let mut rom = io::BufReader::new(fs::File::open(rom)?);

    rom.seek(SeekFrom::Start(FILE_TABLE_LEN_PTR))?;
    let file_table_len = rom.read_u16::<LE>()?;

    fs::create_dir_all(out.as_ref())?;

    for i in 0..u64::from(file_table_len) {
        rom.seek(SeekFrom::Start(i * FILE_ENTRY_SIZE + FILE_TABLE_PTR))?;

        // Read file header info
        let name = util::read_str(&mut rom, 16)?.to_owned();
        let sector = rom.read_u32::<LE>()?;
        let sector_size = rom.read_u32::<LE>()?; // The length of the file in sectors
        let _unk08 = rom.read_u32::<LE>()?; // I'm not sure what this is, original code has this as header size
        let _file_count = rom.read_u32::<LE>()?;

        let path = out.as_ref().join(&name);

        println!("Dumping {} to {}", name, out.as_ref().display());

        let file_offset = u64::from(sector) * SECTOR_SIZE;
        let file_len = u64::from(sector_size) * SECTOR_SIZE;

        let mut out_file = fs::File::create(path)?;
        rom.seek(SeekFrom::Start(file_offset))?;
        let bytes_copied = io::copy(&mut (&mut rom).take(file_len), &mut out_file)?;

        if bytes_copied != file_len {
            panic!("Failed to dump file, did not copy correct file length");
        }
    }

    Ok(())
}

/// Extracts the DAT files from the paths given in `dats` to the directory `out`.
fn extract_dats(dats: Vec<impl AsRef<Path>>, out: impl AsRef<Path>) -> io::Result<()> {
    for d in dats {
        let f = fs::File::open(&d)?;

        let mut sub_count = 0;
        let mut padding_count = 0;

        let dat_dir = out
            .as_ref()
            .join(d.as_ref().file_stem().expect("Invalid DAT name!"));

        for sub in dat::SubIterator::new(f)? {
            let sub = sub.expect("Failed to get subpackage!");
            let mut sub_cursor = io::Cursor::new(&sub.contents);

            let sub_dir = dat_dir.join(format!("sub{}", sub_count));

            fs::create_dir_all(&sub_dir)?;

            if dat::is_subpackage_padding(&mut sub_cursor)? {
                let path = dat_dir.join(format!("padding{}.bin", padding_count));
                println!("Extracting padding{}.bin to {}", padding_count, dat_dir.display());
                let mut out_file = fs::File::create(path)?;
                out_file.write_all(&sub.contents)?;

                padding_count += 1;
            } else {
                for subf in dat::SubFileIterator::new(sub_cursor)? {
                    let subf = subf.expect("Failed to get subfile!");

                    // For now if the file's compressed add another part to extension
                    let path = if subf.compressed() {
                        sub_dir.join(format!("{}.comp", &subf.name))
                    } else {
                        sub_dir.join(&subf.name)
                    };

                    println!("Extracting {} to {}", subf.name, sub_dir.display());
                    let mut out_file = fs::File::create(path)?;
                    out_file.write_all(&subf.contents)?;
                }

                sub_count += 1;
            }
        }
    }

    Ok(())
}

fn main() -> io::Result<()> {
    let matches = App::new("Re: Com Dumper")
        .version("1.0")
        .author("Sirius902 <https://gitlab.com/Sirius902>")
        .about("Dumps, compresses, and decompresses, game files from Kingdom Hearts Re: Chain of Memories")
        .arg(Arg::with_name("rom")
            .short("r")
            .long("rom")
            .value_name("ROM.iso")
            .help("Sets the game iso to dump")
            .takes_value(true))
        .arg(Arg::with_name("output")
            .short("o")
            .long("output")
            .value_name("PATH")
            .help("Where to output the result of the selected operation")
            .takes_value(true))
        .arg(Arg::with_name("decompress")
            .short("d")
            .long("decompress")
            .help("Decompress a game file")
            .takes_value(false))
        .arg(Arg::with_name("compress")
            .short("c")
            .long("compress")
            .help("Compresses a game file")
            .takes_value(false))
        .arg(Arg::with_name("file")
            .short("f")
            .long("file")
            .value_name("FILE")
            .help("A file to compress or decompress using the game's algorithm")
            .takes_value(true))
        .get_matches();

    if let Some(file) = matches.value_of("file") {
        let c = matches.is_present("compress");
        let d = matches.is_present("decompress");

        // If both or neither compress or decompress is selected, complain.
        if !(c ^ d) {
            println!("You must either compress or decompress a file!");
        } else {
            let mut data = vec![];
            fs::File::open(file)?.read_to_end(&mut data)?;

            let operation = if c {
                comp::compress
            } else if d {
                comp::decompress
            } else {
                panic!("Invalid file operation selection!");
            };

            if let Some(output) = matches.value_of("output") {
                fs::create_dir_all(Path::new(&output).parent().expect("Invalid path!"))?;
                fs::File::create(output)?.write_all(&operation(&data))?;
            } else {
                fs::File::create(format!("{}.bak", file))?.write_all(&data)?;
                fs::remove_file(file)?;
                fs::File::create(file)?.write_all(&operation(&data))?;
            }
        }
    } else if let Some(rom) = matches.value_of("rom") {
        let output = matches.value_of("output").unwrap_or("output");
        let iso_output = Path::new(output).join("ISO");
        dump_rom(rom, &iso_output)?;

        // Get an iterator of the DAT file paths from the output of `dump_rom`
        let dats = fs::read_dir(iso_output)?
            .filter_map(Result::ok)
            .map(|entry| entry.path())
            .filter(|path| path.extension().contains(&"DAT"))
            .collect::<Vec<_>>();

        extract_dats(dats, Path::new(output).join("DAT"))?;

        println!("Finished!");
    } else {
        println!("Operation must be specified! Run the program with -h for help.");
    }

    Ok(())
}
