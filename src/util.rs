use std::io;
use std::io::prelude::*;

/// Panics if `buf` contains invalid UTF8.
pub fn str_from_buf<'a>(buf: &'a [u8]) -> &'a str {
    let null_terminator = buf.iter().position(|&x| x == 0).unwrap_or(buf.len());
    std::str::from_utf8(&buf[0..null_terminator]).expect("Invalid UTF8 in buf!")
}

pub fn read_bytes(r: &mut (impl Read), n: usize) -> io::Result<Vec<u8>> {
    let mut buf = vec![0; n];
    r.read_exact(&mut buf).map(|_| buf)
}

macro_rules! read_bytes_array {
    ($r:expr, $n:expr) => {
        {
            let mut buf = [0; $n];
            $r.read_exact(&mut buf).map(|_| buf)
        }
    };
}

pub fn read_str(r: &mut (impl Read), len: usize) -> io::Result<String> {
    Ok(str_from_buf(&read_bytes(r, len)?).to_owned())
}
